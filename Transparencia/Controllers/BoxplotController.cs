﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Model.Model;
using Model.Repository;

namespace Transparencia.Controllers
{
    public class BoxplotController : Controller
    {
        Repository<NumeroTotal> RespNumeroTotal;
        public static string _estado;

        public BoxplotController()
        {
            RespNumeroTotal = new Repository<NumeroTotal>();
        }

        // GET: Estado
        public ActionResult Estado(string id)
        {
            _estado = id;
            //ViewBag.Estado = GetNomeEstado(_estado);
            return View("Estado");
        }
        public IActionResult Boxplot()
        {
            return View();
        }
    }
}