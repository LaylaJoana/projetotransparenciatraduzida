﻿using System;
using System.Collections.Generic;

namespace Model.Model
{
    public partial class ParalamentaresTotalConvenio
    {
        public int Id { get; set; }
        public int Ano { get; set; }
        public int Mes { get; set; }
        public string Uf { get; set; }
        public string NmParlamentar { get; set; }
        public string TipoParlamentar { get; set; }
        public string SitConvenio { get; set; }
        public int QtdConvenio { get; set; }
        public decimal? VlGlobalConv { get; set; }
        
    }
}
