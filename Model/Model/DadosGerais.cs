﻿using System;
using System.Collections.Generic;

namespace Model.Model
{
    public partial class DadosGerais
    {
        public string Local { get; set; }
        public string Parametro { get; set; }
        public int Valor { get; set; }
    }
}
