﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Model.Repository
{
    public interface IRepository<T> where T : class
    {
        void Insert(T item);
        void Save();
        void Update(T item);
        void Delete(Func<T, bool> predicate);
        T Get(Expression<Func<T, bool>> expressao);
        List<T> GetAll();
        List<T> GetAll(Func<T, bool> expressao);
    }
}
