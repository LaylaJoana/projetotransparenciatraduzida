﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Model.Dto
{
    public class Home
    {
        public int QuantidadeParlamentares { get; set; }
        public int QuantidadeConvenios { get; set; }
        public int QuantidadeEmpresas { get; set; }
        public int Pesquisa { get; set; }
    }
}
