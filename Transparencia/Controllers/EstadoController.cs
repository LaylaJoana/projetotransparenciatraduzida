﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model.Model;
using Model.Repository;
using Transparencia.Models;
using Model.Model.Dto;


namespace Transparencia.Controllers
{
    public class EstadoController : Controller
    {
        Repository<NumeroTotal> RespNumeroTotal;
        Repository<ParalamentaresTotalConvenio> RespParlamentarTotalConvenio;
        readonly Repository<SituacaoConvenio> RespSitConvenio;
        public static string _estado;
        public EstadoController()
        {
            RespNumeroTotal = new Repository<NumeroTotal>();
            RespSitConvenio = new Repository<SituacaoConvenio>();
            RespParlamentarTotalConvenio = new Repository<ParalamentaresTotalConvenio>();

        }

        // GET: Estado
        public ActionResult Estado(string id)
        {
            _estado = id;
            ViewBag.Estado = GetNomeEstado(_estado);
            return View("Estado");
        }
        [Route("api/getallCidade")]
        public async Task<JsonResult> CidadeValorTotalConvenio(int ano = 0, int mes = 0)
        {
            var result = RespSitConvenio.ctx.SituacaoConvenio
                .Where(a => a.Uf.Equals(_estado) &&
                      (ano > 0 ? (a.Ano == ano) : true) &&
                      (mes > 0 ? (a.Mes == mes) : true))
                .GroupBy(x => x.Cidade)
                .Select(g => new
                {
                    cidade = g.Key,
                    qtdconvenio = g.Sum(x => x.QtdConvenio),
                    vl_total = g.Sum(x => x.VlGlobalConv)
                    
                });
            return Json(result);
        }

        [Route("api/getall")]
        public async Task<JsonResult> Exibesituacaoporcidade(int ano = 0 , int mes = 0)
        {
            var result = RespSitConvenio.ctx.SituacaoConvenio
                .Where(a => a.Uf.Equals(_estado) && 
                      (ano > 0 ? (a.Ano == ano) : true) &&
                      (mes > 0 ? (a.Mes == mes) : true) )
                .GroupBy(x => x.Cidade)
                .Select(g => new
                {
                    cidade = g.Key,
                    qtdconvenio = g.Sum(x => x.QtdConvenio),
                   

                });
            return Json(result);
        }



        [Route("api/getallParlamentares")]
        public async Task<JsonResult> ExibeParlamentares( int ano = 0, int mes = 0)
        {
            var result = RespParlamentarTotalConvenio.ctx.ParalamentaresTotalConvenio
                .Where(a => a.Uf.Equals(_estado) &&
                      (ano > 0 ? (a.Ano == ano) : true) &&
                      (mes > 0 ? (a.Mes == mes) : true))
                .GroupBy(x => x.NmParlamentar)
                .Select(g => new
                {
                    nome_parlamentar = g.Key,
                    qtdconvenio = g.Sum(x => x.QtdConvenio)
                });
            return Json(result);
        }
        //[Route("api/getallCidadesFavorecidas")]
        //public async Task<JsonResult> ExibeCidadeesFavorecidas(int ano = 0, int mes = 0)
        //{
        //    var result = RespSitConvenio.ctx.SituacaoConvenio
        //        .Where(a => a.Uf.Equals(_estado) &&
        //              (ano > 0 ? (a.Ano == ano) : true) &&
        //              (mes > 0 ? (a.Mes == mes) : true))
        //        .GroupBy(x => x.CidadeFavorecida)
        //        .Select(g => new
        //        {
        //            cidade_favorecida = g.Key,
        //            valor = g.Sum(x => x.VlGlobalConv)
        //        });
        //    return Json(result);
        //}
        [Route("api/getallareas")]
        public async Task<JsonResult> ExibeAreas(int ano = 0, int mes = 0)
        {
            var result = RespSitConvenio.ctx.SituacaoConvenio
                .Where(a => a.Uf.Equals(_estado) &&
                      (ano > 0 ? (a.Ano == ano) : true) &&
                      (mes > 0 ? (a.Mes == mes) : true))
                .GroupBy(x => x.area)
                .Select(g => new
                {
                    area = g.Key,
                    valor = g.Sum(x => x.VlGlobalConv)
                });
            return Json(result);
        }

        [Route("api/Pesquisar")]
        public async Task<JsonResult> Pesquisar(int ano, int mes)
        {
            await Task.Delay(0);
            var Estado = new State();
          
            Estado.totalconvenios = GetTotalConvenios(ano, mes);
            Estado.totalpropostas = GetTotalPropostas(ano, mes);
            Estado.valortotaldascontrapartidas = GetValorTotalContrapartidas(ano, mes).ToString("N");
            Estado.valortotaldaspropostas = GetValorTotaldasPropostas(ano, mes).ToString("N");
            Estado.valortotaldosconvenios = GetValorTotaldosconvenios(ano, mes).ToString("N");
            return Json(Estado);
        }
        
        private string GetNomeCidade(int ano = 0, int mes = 0)
        {
            return FiltrarSituacaoConvenio(ano, mes).ToString();
             
        }

        private decimal GetValorTotaldosconvenios(int ano = 0, int mes = 0)
        {
            return FiltrarNumeroTotal(ano, mes).Sum(a => a.VlTotalConvenio ?? 0);
        }

        private decimal GetValorTotaldasPropostas(int ano = 0, int mes = 0)
        {
            return FiltrarNumeroTotal(ano, mes).Sum(a => a.VlTotalProposta ?? 0);
        }

        private decimal GetValorTotalContrapartidas(int ano = 0, int mes = 0)
        {
            return FiltrarNumeroTotal(ano, mes).Sum(a => a.VlTotalContrapartidaConv ?? 0);
        }

        private int GetTotalPropostas(int ano = 0, int mes = 0)
        {
            return FiltrarNumeroTotal(ano, mes).Sum(a => a.QtdProposta);
        }

        private int GetTotalConvenios(int ano = 0, int mes = 0)
        {
            return FiltrarNumeroTotal(ano, mes).Sum(a => a.QtdConvenio);
        }

        private IQueryable<NumeroTotal> FiltrarNumeroTotal(int ano, int mes)
        {
            var Result = RespNumeroTotal.ctx.NumeroTotal
                            .Where(a =>
                            (ano > 0 ? (a.Ano == ano) : true) &&
                            (mes > 0 ? (a.Mes == mes) : true) &&
                            a.Uf.Equals(_estado));
            return Result;
        }

        private IQueryable<SituacaoConvenio> FiltrarSituacaoConvenio(int ano, int mes)
        {
            var Result = RespSitConvenio.ctx.SituacaoConvenio.Where(a =>
                             (ano > 0 ? (a.Ano == ano) : true) &&
                             (mes > 0 ? (a.Mes == mes) : true) &&
                             a.Uf.Equals(_estado));

            return Result;
        }

        string GetNomeEstado(string UF)
        {
            var Nome = "";
            switch (UF.ToUpper())
            {
                case "AC": Nome = "Acre"; break;
                case "AL": Nome = "Alagoas"; break;
                case "AM": Nome = "Amazonas"; break;
                case "AP": Nome = "Amapá"; break;
                case "BA": Nome = "Bahia"; break;
                case "CE": Nome = "Ceará"; break;
                case "DF": Nome = "Distrito Federal"; break;
                case "ES": Nome = "Espírito Santo"; break;
                case "GO": Nome = "Goiás"; break;
                case "MA": Nome = "Maranhão"; break;
                case "MG": Nome = "Minas Gerais"; break;
                case "MS": Nome = "Mato Grosso do Sul"; break;
                case "MT": Nome = "Mato Grosso"; break;
                case "PA": Nome = "Pará"; break;
                case "PB": Nome = "Paraíba"; break;
                case "PE": Nome = "Pernambuco"; break;
                case "PI": Nome = "Piauí"; break;
                case "PR": Nome = "Paraná"; break;
                case "RJ": Nome = "Rio de Janeiro"; break;
                case "RN": Nome = "Rio Grande do Norte"; break;
                case "RO": Nome = "Rondônia"; break;
                case "RR": Nome = "Roraima"; break;
                case "RS": Nome = "Rio Grande do Sul"; break;
                case "SC": Nome = "Santa Catarina"; break;
                case "SE": Nome = "Sergipe"; break;
                case "SP": Nome = "São Paulo"; break;
                case "TO": Nome = "Tocantíns"; break;
            }
            return Nome;
        }
    }
}