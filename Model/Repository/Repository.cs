﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Model.Repository
{
    public enum OpcoesRegistro
    {
        MoverParaPrimeiroRegistro = 0,
        MoverParaUltimoRegistro = 1,
        MoverParaRegistroAnterior = 2,
        MoverParaProximoRegistro = 3
    }

    public class Repository<T> : IDisposable, IRepository<T> where T : class
    {
        public DbContextModel ctx = new DbContextModel();

        public Repository()
        { }

        public T Get(Expression<Func<T, bool>> expressao)
        {
            return ctx.Set<T>().Where(expressao).AsNoTracking().FirstOrDefault();
        }

        public List<T> GetAll(Func<T, bool> expressao)
        {
            return ctx.Set<T>().Where(expressao).AsEnumerable().ToList();
        }


        public List<T> GetAll()
        {
            return ctx.Set<T>().ToList();
        }

        public void Insert(T item)
        {
            ctx.Set<T>().Add(item);
        }

        internal object Where(Func<object, object> p)
        {
            throw new NotImplementedException();
        }

        public void Update(T item)
        {
            ctx.Set<T>().Update(item);
        }

        public void Delete(Func<T, bool> predicate)
        {
            ctx.Set<T>()
               .Where(predicate).ToList()
               .ForEach(del => ctx.Set<T>().Remove(del));
        }

        public void Delete(T obj)
        {
            ctx.Set<T>().Remove(obj);
        }

        public void Save()
        {
            ctx.SaveChanges();
        }

        public void ExecutarComando(string sql)
        {
            ctx.Database.ExecuteSqlCommand(sql);
        }

        public void Dispose()
        {
            ctx.Dispose();
        }

        public PropertyInfo FindField(string cmp = null)
        {
            PropertyInfo pro = null;
            if (cmp == null)
            {
                pro = typeof(T).GetProperties().SingleOrDefault(p => p.IsDefined(typeof(KeyAttribute)));
                if (pro == null)
                {
                    pro = typeof(T).GetProperties().FirstOrDefault(p => p.Name.ToUpper().StartsWith("ID"));
                }
            }
            else
            {
                pro = typeof(T).GetProperties().FirstOrDefault(p => p.Name.ToString().ToUpper().Equals(cmp.ToUpper()));
            }
            return pro;
        }

        public Task ComitarAsync()
        {
            return ctx.SaveChangesAsync();
        }

        public Task<List<T>> ConsultarAsync(Expression<Func<T, bool>> Expresao, int Qtd = 0)
        {
            return ctx.Set<T>().Where(Expresao).ToListAsync();
        }

        public Task<List<T>> ConsultarOtimizadaAsync(Expression<Func<T, bool>> Expresao, Expression<Func<T, dynamic>> OrderBy = null, string order = "asc", int Qtd = 0)
        {
            IQueryable<T> model = ctx.Set<T>();
            if (Expresao != null)
                model = model.Where(Expresao).AsQueryable();
            if (OrderBy != null)
            {
                model = order.Equals("desc") ? model.OrderByDescending(OrderBy).AsQueryable() : model.OrderBy(OrderBy).AsQueryable();
            }

            if (Qtd > 0)
            {
                model = model.Take(Qtd).AsQueryable();
            }
            return model.ToListAsync();
        }

        public Task<T> SelecionarAsync(Expression<Func<T, bool>> Expresao)
        {
            return ctx.Set<T>().FirstOrDefaultAsync(Expresao);
        }
    }
}
