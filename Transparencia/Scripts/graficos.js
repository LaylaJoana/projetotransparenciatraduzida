
    var ctx = document.getElementById('myChart').getContext('2d');
    var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'bar',

    // The data for our dataset
    data: {
        labels: ["Item 1", "Item 2", "Item 3", "Item 4", "Item 5"],
        datasets: [
        {
            label: "Primeiro",
            data: [0, 50, 60, 70, 80],
            backgroundColor: '#333',
            borderColor: 'transparent',
        },
        {
            label: "Segundo",
            data: [50, 60, 70, 80, 90],
            backgroundColor: '#605ca8',
            borderColor: 'transparent',
        }
        ]
    },

    // Configuration options go here
    options: {}
    });



    var ctx = document.getElementById('myChart2').getContext('2d');
    var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'doughnut',

    // The data for our dataset
    data: {
        labels: ["Item 1", "Item 2", "Item 3", "Item 4", "Item 5"],
        datasets: [
        {
            label: "Primeiro",
            data: [0, 50, 60, 70, 80],
            backgroundColor: [
                '#605ca8',
                '#333',
                '#605ca8',
                '#333',
                '#605ca8'
            ],
            borderColor: '#fff',
        },
        ]
    },

    // Configuration options go here
    options: {}
    });
