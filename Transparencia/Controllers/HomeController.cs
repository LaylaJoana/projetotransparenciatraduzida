﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Model.Model;
using Model.Repository;
using Transparencia.Models;
using Model.Model.Dto;
using System.Threading.Tasks;

namespace Transparencia.Controllers
{
    
    public class HomeController : Controller
    {
        Repository<DadosGerais> RespDadosGerais;
        Repository<NumeroTotal> RespNumeroTotal;
        Repository<SituacaoConvenio> RespSituacaoConvenio;

        public HomeController()
        {
            RespDadosGerais = new Repository<DadosGerais>();
            RespNumeroTotal = new Repository<NumeroTotal>();
            RespSituacaoConvenio = new Repository<SituacaoConvenio>();
          
        }

        public IActionResult Index()
        {
            var Home = new Home();
           // Home.Pesquisa = GetPesquisa();
            Home.QuantidadeEmpresas = GetQuantidadeEmpresa();
            Home.QuantidadeConvenios = GetQuantidadeConvenios();
            Home.QuantidadeParlamentares = GetQuantidadeParlamentares();
            return View("Index", Home);
        }

        public List<string> Pesquisar(string campo)
        {

            return RespNumeroTotal.ctx.NumeroTotal
                .Where(p => p.Uf.StartsWith(campo, StringComparison.OrdinalIgnoreCase))
                .Select(p => p.Uf).ToList();

        }
        

        [HttpPost]
        [Route("api/PesquisarEstadoParlamentar")]

        public JsonResult Search (string prefix)
        {
            prefix = prefix.ToUpper();
            var retornoList = (from N in RespNumeroTotal.ctx.NumeroTotal.ToList()
                               where N.Uf.StartsWith(prefix)
                               select new{N.Uf}).Distinct();
            return Json(retornoList);
        }
       

     


        public IActionResult About()
        {
            var RespDadosGerais = new Repository<DadosGerais>();
            var x = RespDadosGerais.GetAll(a => a.Valor.Equals("ffdf"));
            ViewData["Message"] = "DadosGerais : " + x.Count();
            return View();
        }

        public IActionResult Contato()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

       

        public IActionResult sobre()
        {
            return View();

        }

        
        private int GetQuantidadeParlamentares()

        {

             
             var result = RespDadosGerais.ctx.DadosGerais.Where(a => a.Parametro.Equals("qtd_parlamentares")).Select(a => a.Valor);

            
            //var paramentro = RespDadosGerais.ctx.DadosGerais.Select(a => a.Valor).Where(b => b.Parametro.Equals("qtd_parlamentares"));
            //RespDadosGerais.ctx.DadosGerais.Sum(a => a.Valor);
            //rresult.Count();
            return result.Sum();

           
           
           
        }

        private int GetQuantidadeConvenios()
        {
            var result =  RespDadosGerais.ctx.DadosGerais.Where(a => a.Parametro.Equals("qtd_convenios")).Select(a => a.Valor);
            return result.Sum();

   
        }

        private int GetQuantidadeEmpresa()
        {
            var result = RespDadosGerais.ctx.DadosGerais.Where(a => a.Parametro.Equals("qtd_empresas")).Select(a => a.Valor);
            return result.Sum();
        }


    }
}
