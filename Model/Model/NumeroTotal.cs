﻿using System;
using System.Collections.Generic;

namespace Model.Model
{
    public partial class NumeroTotal
    {
        public int Ano { get; set; }
        public int Mes { get; set; }
        public string Uf { get; set; }
        public int NrHabit { get; set; }
        public int QtdProposta { get; set; }
        public int QtdConvenio { get; set; }
        public decimal? VlTotalProposta { get; set; }
        public decimal? VlTotalConvenio { get; set; }
        public decimal? VlTotalContrapartidaConv { get; set; }
        public int TotalEmpresas { get; set; }
    }
}
