﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Model.Model;

namespace Model.Repository
{
    public partial class DbContextModel : DbContext
    {
        public virtual DbSet<DadosGerais> DadosGerais { get; set; }
        public virtual DbSet<NumeroTotal> NumeroTotal { get; set; }
        public virtual DbSet<ParalamentaresTotalConvenio> ParalamentaresTotalConvenio { get; set; }
        public virtual DbSet<SituacaoConvenio> SituacaoConvenio { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(@"Data Source=DESKTOP-S5DNJIU\SQLEXPRESS;Initial Catalog=minerado;Integrated Security=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DadosGerais>(entity =>
            {
                entity.HasKey(e => new { e.Local, e.Parametro });

                entity.ToTable("dados_gerais");

                entity.Property(e => e.Local)
                    .HasColumnName("local")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Parametro)
                    .HasColumnName("parametro")
                    .HasMaxLength(30)
                    .IsUnicode(false);

             

            });

            modelBuilder.Entity<NumeroTotal>(entity =>
            {
                entity.HasKey(e => new { e.Ano, e.Mes, e.Uf });

                entity.ToTable("numero_total");

                entity.Property(e => e.Ano).HasColumnName("ano");

                entity.Property(e => e.Mes).HasColumnName("mes");

                entity.Property(e => e.Uf)
                    .HasColumnName("uf")
                    .HasColumnType("char(2)");

                entity.Property(e => e.NrHabit).HasColumnName("nr_habit");

                entity.Property(e => e.QtdConvenio).HasColumnName("qtd_convenio");

                entity.Property(e => e.QtdProposta).HasColumnName("qtd_proposta");

                entity.Property(e => e.VlTotalContrapartidaConv)
                    .HasColumnName("vl_total_contrapartida_conv")
                    .HasColumnType("numeric(17, 2)");

                entity.Property(e => e.VlTotalConvenio)
                    .HasColumnName("vl_total_convenio")
                    .HasColumnType("numeric(17, 2)");

                entity.Property(e => e.VlTotalProposta)
                    .HasColumnName("vl_total_proposta")
                    .HasColumnType("numeric(17, 2)");

                entity.Property(e => e.TotalEmpresas).HasColumnName("total_empresas");
           




            });

            modelBuilder.Entity<ParalamentaresTotalConvenio>(entity =>
            {
                entity.ToTable("paralamentares_total_convenio");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Ano).HasColumnName("ano");

                entity.Property(e => e.Mes).HasColumnName("mes");

                entity.Property(e => e.NmParlamentar)
                    .IsRequired()
                    .HasColumnName("nm_parlamentar")
                    .HasMaxLength(100)
                    .IsUnicode(false); 

                entity.Property(e => e.QtdConvenio).HasColumnName("qtd_convenio");

                entity.Property(e => e.SitConvenio)
                    .IsRequired()
                    .HasColumnName("sit_convenio")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TipoParlamentar)
                    .IsRequired()
                    .HasColumnName("tipo_parlamentar")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Uf)
                    .IsRequired()
                    .HasColumnName("uf")
                    .HasColumnType("char(2)");

                entity.Property(e => e.VlGlobalConv)
                    .HasColumnName("vl_global_conv")
                    .HasColumnType("numeric(17, 2)");
            });

            modelBuilder.Entity<SituacaoConvenio>(entity =>
            {
                entity.ToTable("situacao_convenio");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Ano).HasColumnName("ano");

                entity.Property(e => e.Mes).HasColumnName("mes");

                entity.Property(e => e.QtdConvenio).HasColumnName("qtd_convenio");

                entity.Property(e => e.SitConvenio)
                    .IsRequired()
                    .HasColumnName("sit_convenio")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Uf)
                    .IsRequired()
                    .HasColumnName("uf")
                    .HasColumnType("char(2)");

                entity.Property(e => e.VlGlobalConv)
                    .HasColumnName("vl_global_conv")
                    .HasColumnType("numeric(17, 2)");

                entity.Property(e => e.Cidade)
                    .IsRequired()
                    .HasColumnName("cidade")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                //entity.Property(e => e.CidadeFavorecida)
                //   .IsRequired()
                //   .HasColumnName("cidade_favorecida")
                //   .HasMaxLength(100)
                //   .IsUnicode(false);

                entity.Property(e => e.area)
                .IsRequired()
                .HasColumnName("area")
                .HasMaxLength(100)
                .IsUnicode(false);

            });
        }
    }
}
