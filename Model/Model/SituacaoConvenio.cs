﻿using System;
using System.Collections.Generic;

namespace Model.Model
{
    public partial class SituacaoConvenio
    {
        public int Id { get; set; }
        public int Ano { get; set; }
        public int Mes { get; set; }
        public string Uf { get; set; }
        public string SitConvenio { get; set; }
        public int QtdConvenio { get; set; }
        public decimal? VlGlobalConv { get; set; }
        public string Cidade { get; set; }
        //public string CidadeFavorecida { get; set; }
        public string area { get; set; }
    }
}
