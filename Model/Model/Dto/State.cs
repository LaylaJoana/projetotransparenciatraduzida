﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Model.Dto
{
   public class State
    {
        // Numero Total ....view estado
        public int totalpropostas { get; set; }
        public int totalconvenios { get; set; }
        public string valortotaldaspropostas { get; set; }
        public string valortotaldosconvenios { get; set; }
        public string valortotaldascontrapartidas { get; set; }

        // Situação dos Convenios por cidades

        
        public int TotalConveniosPorcidade { get; set; }


        // Parlamentares

        public string Parlamentar { get; set; }
        public int  QuantidadeConvenioPorParlamentar { get; set; }

        // Areas Favorecidas

        public string AreaFavorecida { get; set; }
        public int QuantidadeConvenioPorarea { get; set; }
        public decimal valorTotalConvenioPorarea { get; set; }

        // Cidades

        public string Nomecidade { get; set; }
        public int QtdConveniocidade { get; set; }
        public decimal ValorTotalConvenioCidada { get; set; }

        // Favorecidos

        public string CidadeFavorecida { get; set; }
        public decimal ValorTotalCidadeFavorecida { get; set; }

    }
}
